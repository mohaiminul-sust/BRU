/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bru;

import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author Andromeda
 */
public class MetaObject {

    String fileName;
    Path filePath;
    boolean isDir;
    boolean isHidden;
    boolean isRegular;
    boolean isSymlink;
    long size;
    String owner;
    FileTime time;

    public MetaObject() {

        this.fileName = "";
        this.filePath = null;
        this.isDir = false;
        this.isHidden = false;
        this.isRegular = false;
        this.isSymlink = false;
        this.owner = "";
        this.size = 0;
        this.time = null;
    }
    
    public MetaObject(String fname, Path fpath, boolean dir, boolean hidden,
            boolean regular, boolean symlink, long size, String owner, FileTime time) {

        this.fileName = fname;
        this.filePath = fpath;
        this.isDir = dir;
        this.isHidden = hidden;
        this.isRegular = regular;
        this.isSymlink = symlink;
        this.owner = owner;
        this.size = size;
        this.time = time;
    }
    
    public String getDate() {
        if (this.time != null) {
            return extractDate(time);
        } else {
            return "";
        }
    }

    public String getTime() {
        if (this.time != null) {
            return extractTime(time);
        } else {
            return "";
        }
    }
    
    public String getSize() {
        if (this.size > 0) {
            return readableFileSize(size);
        } else {
            return "0";
        }
    }

    private String extractDate(FileTime time) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        return formatter.format(time.toMillis());
    }

    private String extractTime(FileTime time) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss a");
        return formatter.format(time.toMillis());
    }

    private String readableFileSize(long size) {
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }
}
