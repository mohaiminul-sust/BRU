/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bru;

import java.io.IOException;
import java.nio.file.Files;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;

/**
 *
 * @author Andromeda
 */
public class MetaManager {
    
    private String filepath="";
    private Path file;
    
    public MetaManager(String filepath){
        this.filepath = filepath;
        this.file = Paths.get(this.filepath);
    }
    
    public MetaObject getAttributes() throws IOException {
        String fileName = file.toString();
        Path filePath = file.toAbsolutePath();
        boolean isDir = Files.isDirectory(file);
        boolean isHidden = Files.isHidden(file);
        boolean isRegular = Files.isRegularFile(file, NOFOLLOW_LINKS);
        boolean isSymlink = Files.isSymbolicLink(file);
        long size = Files.size(file);
        String owner = Files.getOwner(file).getName();
        FileTime time = Files.getLastModifiedTime(file);
        
        MetaObject metaobj = new MetaObject(fileName, filePath, isDir, isHidden, isRegular, isSymlink, size, owner, time);
        
        return metaobj;
    }
}
