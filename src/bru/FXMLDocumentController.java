/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bru;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Window;

/**
 *
 * @author Andromeda
 */
public class FXMLDocumentController implements Initializable {
    
    private final Desktop desktop = Desktop.getDesktop();
    private ObservableList<String> listItems;
    private final HashMap<String, String> filePaths = new HashMap<>();
    private RenameUtil BRU;
    private AlertBox alert;
    
    @FXML
    public Window stage;
    public ListView fileList;
    public Button removeButton;
    public Button removeAllButton;
    public TextField patternHeader;
    public TextField startingAt;
    public TextField nameSeparator;
    public ComboBox patternLocation;
    public TextField numberOfDigits;
    public Accordion optionContainer;
    public TitledPane renameIndexedPane;
    public TitledPane renameNRenamePane;    
    public TitledPane renameMetaPane;
    public TextField numChar;
    public ComboBox startLoc;
    public ComboBox patternMode;
    public TextField toAppend;
    public TextField metaOwner;
    public TextField metaDate;
    public TextField metaTime;
    public TextField metaSize;
    public TextField metaPattern;
    public TextField metaSeparator;
    public CheckBox metaOwnerCB;
    public CheckBox metaDateCB;
    public CheckBox metaTimeCB;
    public CheckBox metaSizeCB;
    public CheckBox metaPatternCB;
    
    @FXML
    private void handleExitFileMenuAction(ActionEvent event) {
        System.exit(0);
    }
    
    @FXML
    private void handleOpenFileMenuAction(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        configureFileChooser(fileChooser, "Open files for renaming");
        final List<File> files = fileChooser.showOpenMultipleDialog(stage);

        //clear and enable list view
        fileList.setItems(null);
        fileList.setDisable(false);
        listItems = FXCollections.observableArrayList();
        filePaths.clear();
        
        for (File file : files) {
            String fileName = file.getName();
            String filePath = file.getPath();
            listItems.add(fileName);
            filePaths.put(fileName, filePath);
        }
        
        fileList.setItems(listItems);
        printHashMap(); //debug
    }
    
    @FXML
    private void handleOpenDirectoryFileMenuAction(ActionEvent event) {
        final DirectoryChooser dirChooser = new DirectoryChooser();
        final File selectedDir = dirChooser.showDialog(stage);
        
        if (selectedDir != null) {
            String path = selectedDir.getAbsolutePath();
            File dir = new File(path);
            
            if (dir.isDirectory()) {
                File[] files = dir.listFiles();

                //clear and enable list view
                fileList.setItems(null);
                fileList.setDisable(false);
                listItems = FXCollections.observableArrayList();
                filePaths.clear();

                //add file names to list view
                for (File file : files) {
                    String fileName = file.getName();
                    String filePath = file.getPath();
                    listItems.add(fileName);
                    filePaths.put(fileName, filePath);
                }
                
                fileList.setItems(listItems);
                printHashMap(); //debug
            }
        }
    }
    
    @FXML
    private void handleIndexedRenameFileAction(ActionEvent event) {
        ObservableList<String> list = fileList.getItems();
        
        if (list.contains("No files or directory selected")) {
            //dialogue pane code
            alert = new AlertBox();
            alert.errorBox("List has no files!!", "No files Selected!!", "Select Directory");
            return;
        }
        
        List<String> paths = new ArrayList<String>();
        
        for (String listItem : list) {
            if (filePaths.containsKey(listItem)) {
                String mapItem = filePaths.get(listItem);
                String path = getPath(mapItem);
                paths.add(path);
            } else {
                System.err.println("Item not in hashmap !");
            }
        }
        
        String pattern = patternHeader.getText();
        String location = patternLocation.getValue().toString();
        String separator = nameSeparator.getText().toString();
        int startsAt = Integer.valueOf(startingAt.getText());
        int numDigits = Integer.valueOf(numberOfDigits.getText());
        
        if (pattern.isEmpty()) {
            separator = "";
        }
        
        System.out.println("Paths to rename: " + paths);

        //call renamer func from RenameUtil Class
        BRU = new RenameUtil(paths);
        BRU.IndexedRenaming(startsAt, pattern, location, separator, numDigits);
        
        
        //repopulate listView with changed files
        initList();
        
        alert = new AlertBox();
        alert.successBox("Renaming was successfull!!", "Done", "Success");
    }
    
    @FXML
    private void handleNRenameFileAction(ActionEvent event) {
        ObservableList<String> list = fileList.getItems();
        
        if (list.contains("No files or directory selected")) {
            //dialogue pane code
            alert = new AlertBox();
            alert.errorBox("List has no files!!", "No files Selected!!", "Select Directory");
            
            return;
        }
        
        List<String> paths = new ArrayList<String>();
        
        for (String listItem : list) {
            if (filePaths.containsKey(listItem)) {
                String mapItem = filePaths.get(listItem);
                String path = getPath(mapItem);
                paths.add(path);
            } else {
                System.err.println("Item not in hashmap !");
            }
        }
        
        int numCh = Integer.parseInt(numChar.getText());
        String mode = patternMode.getValue().toString();
        String sLoc = startLoc.getValue().toString();
        String stAppend = toAppend.getText();
        
        System.out.println("Paths to rename: " + paths);

        //numChar exception catcher
        if (numCh < 0) {
            System.out.println("Invalid");
            return;
        }
        //call renamer func from RenameUtil Class
        BRU = new RenameUtil(paths);
        BRU.nRenaming(numCh, mode, sLoc, stAppend);

        //repopulate listView with changed files
        initList();
            
        alert = new AlertBox();
        alert.successBox("Renaming was successfull!!", "Done", "Success");
    
    }
    
    @FXML
    private void handleListMenuRemoveAction(ActionEvent event) {
        List<String> fileNames = new ArrayList<>();
        fileNames = fileList.getSelectionModel().getSelectedItems();
        
        Iterator listIter = fileNames.iterator();
        
        while (listIter.hasNext()) {
            String fileName = listIter.next().toString();
            filePaths.remove(fileName);
        }
        
        listItems.clear();
        listItems.addAll(filePaths.keySet());
        fileList.setItems(listItems);

        //Empty List Handler
        if (listItems.isEmpty()) {
            initList();
        }
        
        printHashMap(); //debug
    }
    
    @FXML
    private void handleListMenuRemoveAllAction(ActionEvent event) {
        initList();
    }
    
    
    @FXML
    private void handleMetaAppendAction(ActionEvent event) {
        ObservableList<String> list = fileList.getItems();
        
        if (list.contains("No files or directory selected")) {
            //dialogue pane code
            alert = new AlertBox();
            alert.errorBox("List has no files!!", "No files Selected!!", "Select Directory");
            
            return;
        }
        
        List<String> paths = new ArrayList<String>();
        HashMap<String, MetaObject> metamap = new HashMap<>();
        
        for (String listItem : list) {
            if (filePaths.containsKey(listItem)) {
                String mapItem = filePaths.get(listItem);
                String path = getPath(mapItem);
                paths.add(path);
                
                MetaManager mgr = new MetaManager(path);
                try {
                    metamap.put(path, mgr.getAttributes());
                } catch (IOException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.err.println("Item not in hashmap !");
            }
        }
        
        //cb stats
        boolean metaOwnerCBState = metaOwnerCB.isSelected();
        boolean metaDateCBState = metaDateCB.isSelected();
        boolean metaTimeCBState = metaTimeCB.isSelected();
        boolean metaSizeCBState = metaSizeCB.isSelected();
        boolean metaPatternCBState = metaPatternCB.isSelected();
        
        String pattern = metaPattern.getText();
        String separator = metaSeparator.getText();
        
        //call renamer func from RenameUtil Class
        BRU = new RenameUtil(paths, metamap);
        BRU.metaRenaming(metaOwnerCBState, metaDateCBState, metaTimeCBState, metaSizeCBState, metaPatternCBState, pattern, separator);

        //repopulate listView with changed files
        initList();
        
        alert = new AlertBox();
        alert.successBox("Renaming was successfull!!", "Done", "Success");
    }
    
    @FXML
    private void handleMouseClickListViewItems(MouseEvent arg0) {
//        System.out.println("Ping");
        String path = filePaths.get(fileList.getSelectionModel().getSelectedItem());
        
        MetaManager metamgr = new MetaManager(path);
        
        try {
            MetaObject obj = metamgr.getAttributes();

            //set values
            metaOwner.setText(obj.owner);
            metaDate.setText(obj.getDate());
            metaTime.setText(obj.getTime());
            metaSize.setText(obj.getSize());
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initAccordianIR();
        initAccordianNR();
        initAccordianMR();
        initAccordian();
        initList();
    }

    //empty file list and listItems
    private void initList() {
        listItems = FXCollections.observableArrayList("No files or directory selected");
        filePaths.clear();
        fileList.setItems(listItems);
        fileList.setDisable(true);
        fileList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        printHashMap(); //debug
    }
    
    private void initAccordian() {
        optionContainer.setExpandedPane(renameIndexedPane);
    }
    
    private void initAccordianIR() {
        //pattern header
        patternHeader.setText("test");

        //location
        ObservableList<String> patternLocationChoices = FXCollections.observableArrayList("Before Index", "After Index");
        patternLocation.setItems(patternLocationChoices);
        patternLocation.setValue(patternLocationChoices.get(0));

        //index starting at
        startingAt.setText("1");

        //# of digits
        numberOfDigits.setText("2");

        //set separator
        nameSeparator.setText("_");
    }
    
    private void initAccordianNR() {
        
        numChar.setText("");
        
        ObservableList<String> startLocs = FXCollections.observableArrayList("Start", "End");
        startLoc.setItems(startLocs);
        startLoc.setValue(startLocs.get(0));
        
        ObservableList<String> renameModes = FXCollections.observableArrayList("Remove", "Append");
        patternMode.setItems(renameModes);
        patternMode.setValue(renameModes.get(0));
        
        toAppend.setDisable(true);
        
    }
    
    private void initAccordianMR() {
        metaOwner.setEditable(false);
        metaDate.setEditable(false);
        metaTime.setEditable(false);
        metaSize.setEditable(false);
        
        metaOwner.setText("Select File");
        metaDate.setText("Select File");
        metaSize.setText("Select File");
        metaTime.setText("Select File");
        metaPattern.setText("");
        metaSeparator.setText("_");
        
        metaDateCB.setSelected(false);
        metaOwnerCB.setSelected(false);
        metaSizeCB.setSelected(false);
        metaTimeCB.setSelected(false);
        metaPatternCB.setSelected(false);
    }
    
    @FXML
    private void handleModeComboAction(ActionEvent event) {
        
        String mode = patternMode.getValue().toString();
        
        if (mode.equals("Remove")) {
            toAppend.setDisable(true);
            numChar.setDisable(false);
            numChar.requestFocus();
        } else if (mode.equals("Append")) {
            numChar.setDisable(true);
            toAppend.setDisable(false);
            toAppend.requestFocus();
        }
    }
    
    @FXML
    private void handleRenameFileMenuAction(ActionEvent event){
        String name = optionContainer.getExpandedPane().getText();
        
        if(null != name) //global renamer context
        switch (name) {
            case "Indexed Renaming":
                handleIndexedRenameFileAction(event);
                break;
            case "Bulk 'n' Renaming":
                handleNRenameFileAction(event);
                break;
            case "Meta Appender":
                handleMetaAppendAction(event);
                break;
            default:
                break;
        }
    }
    
    private static void configureFileChooser(final FileChooser fileChooser, String title) {
        fileChooser.setTitle(title);
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        
    }
    
    private static String getPath(String mapItem) {
        String path = "";
        
        try {
            path = mapItem.substring(mapItem.lastIndexOf("=") + 1);
            return path;
        } catch (Exception ex) {
            System.out.println("Error : " + ex);
            return path;
        }
    }
    
    private void openFile(File file) {
        try {
            desktop.open(file);
        } catch (IOException ex) {
            Logger.getLogger(
                    this.getClass().getName()).log(
                            Level.SEVERE, null, ex
                    );
        }
    }
    
    private void printHashMap() {
        System.out.println(filePaths);
    }    
}
